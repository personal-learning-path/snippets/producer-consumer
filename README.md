# Producer / Consumer pattern
Simple example for demonstrating the Producer / Consumer pattern using `Lock` and `Condition` for synchronizing threads.

## Test case
Our producers and consumers will work on a simple `String[]` array which further is called **bucket** and they will 
read/write always from  position `[0]` of the array (for simplicity).
Both producer and consumer are using a lock and two conditions. The producer is not allowed to produce new values until
the bucket *is not empty* and the consumer is not allowed to read values until the bucket *is not full*. In simple terms:
 - **is not empty** translates to `bucket[0] != null`
 - **is not full** translates to `bucket[0] == null`  
 
 The producer and consumer implement the `Callable` interface because we want to return a result at the end of the execution.
 > Remember the classes implementing `Callable` interface can be interrupted and can return a result at the end of execution.
 

## Producer
The producer will be responsible of **producing** messages and for our particular case it will just simply set a random
value.

```java
    @Override
    public String call() throws InterruptedException {
        int count = 10;

        while (count > 0) {
            try {
                lock.lock();
                while (isFull(bucket)) {
                    fullBucket.await();
                }
                bucket[0] = "produced-" + count;
                System.out.println(format("{%s} Produced message={%s}", id, bucket[0]));
                count--;
                isEmpty.signalAll();
            } finally {
                lock.unlock();
            }
        }

        return format("Producer %s finished", id);
    }
```

## Consumer
The consumer will be responsible of **consuming** messages and for our particular case it will just print out the value
and set the position to `null`.

```java
    @Override
    public String call() throws InterruptedException {
        int count = 10;

        while (count > 0) {
            try {
                lock.lock();
                while (isEmpty(bucket)) {
                    isEmpty.await();
                }
                System.out.println(format("{%s} Consumed message={%s}", id, bucket[0]));
                bucket[0] = null;
                count--;
                fullBucket.signalAll();
            } finally {
                lock.unlock();
            }
        }

        return format("Consumer %s finished", id);
    }
```

## Executor Service
An executor service is created having a pool of 4 threads (2 producers and 2 consumers) with the following command 
`final ExecutorService executor = Executors.newFixedThreadPool(4);`. In case of an execution exception when the producers
and consumers are being executed the JVM will not stop the executor service automatically and it will continue to run. To
avoid this behavior we need to manually shutdown the executor via a `finally` block.

## Main
```java
        // initialize the lock and condition
        final Lock lock = new ReentrantLock();
        final Condition isEmpty = lock.newCondition();
        final Condition fullBucket = lock.newCondition();

        // the bucket to which the producers will write and the consumers will read
        final String[] bucket = new String[1];

        // create an executor with a pool consisting of 4 threads
        final ExecutorService executor = Executors.newFixedThreadPool(4);

        // create the producers
        final Producer producerOne = new Producer(bucket, lock, isEmpty, fullBucket);
        final Producer producerTwo = new Producer(bucket, lock, isEmpty, fullBucket);

        // create the consumers
        final Consumer consumerOne = new Consumer(bucket, lock, isEmpty, fullBucket);
        final Consumer consumerTwo = new Consumer(bucket, lock, isEmpty, fullBucket);

        // create the tasks
        final List<Callable<String>> tasks =
                new ArrayList<>(Arrays.asList(producerOne, producerTwo, consumerOne, consumerTwo));

        try {
            final List<Future<String>> futures = executor.invokeAll(tasks);

            futures.forEach(f -> {
                try {
                    System.out.println(f.get());
                } catch (final ExecutionException | InterruptedException e) {
                    System.out.println(format("An exception occurred \n %s", e.getMessage()));
                }
            });
        } finally {
            System.out.println("Executor stopped");
            executor.shutdown();
        }
```

