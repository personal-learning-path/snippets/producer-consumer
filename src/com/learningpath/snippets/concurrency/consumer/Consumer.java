package com.learningpath.snippets.concurrency.consumer;

import static java.lang.String.format;

import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * The consumer.
 *
 * @author George
 */
public class Consumer implements Callable<String> {

    private String id;
    private String[] bucket;
    private Lock lock;
    private Condition consumeNewMessage;
    private Condition produceNewMassage;

    public Consumer(final String[] b, final Lock l, final Condition consumeNewMessage, final Condition produceNewMassage) {
        this.bucket = b;
        this.lock = l;
        this.consumeNewMessage = consumeNewMessage;
        this.produceNewMassage = produceNewMassage;
        this.id = UUID.randomUUID().toString();
    }

    @Override
    public String call() throws InterruptedException {
        int count = 10;

        while (count > 0) {
            try {
                lock.lock();
                while (isEmpty(bucket)) {
                    consumeNewMessage.await();
                }
                System.out.println(
                        format("{Thread: %s} | {id: %s} Consumed message={%s}", Thread.currentThread().getName(), id,
                                bucket[0]));
                bucket[0] = null;
                count--;
                produceNewMassage.signalAll();
            } finally {
                lock.unlock();
            }
        }

        return format("Consumer %s finished", id);
    }

    private boolean isEmpty(final String[] bucket) {
        return bucket[0] == null;
    }
}
