package com.learningpath.snippets.concurrency;

import com.learningpath.snippets.concurrency.consumer.Consumer;
import com.learningpath.snippets.concurrency.producer.Producer;

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        // initialize the lock and condition
        final Lock lock = new ReentrantLock();
        final Condition consumeNewMessage = lock.newCondition();
        final Condition produceNewMassage = lock.newCondition();

        // the bucket to which the producers will write and the consumers will read
        final String[] bucket = new String[1];

        // create an executor with a pool consisting of 4 threads
        final ExecutorService executor = Executors.newFixedThreadPool(4);

        // create the producers
        final Producer producerOne = new Producer(bucket, lock, consumeNewMessage, produceNewMassage);
        final Producer producerTwo = new Producer(bucket, lock, consumeNewMessage, produceNewMassage);

        // create the consumers
        final Consumer consumerOne = new Consumer(bucket, lock, consumeNewMessage, produceNewMassage);
        final Consumer consumerTwo = new Consumer(bucket, lock, consumeNewMessage, produceNewMassage);

        // create the tasks
        final List<Callable<String>> tasks =
                new ArrayList<>(Arrays.asList(producerOne, producerTwo, consumerOne, consumerTwo));

        try {
            final List<Future<String>> futures = executor.invokeAll(tasks);

            futures.forEach(f -> {
                try {
                    System.out.println(f.get());
                } catch (final ExecutionException | InterruptedException e) {
                    System.out.println(format("An exception occurred \n %s", e.getMessage()));
                }
            });
        } finally {
            System.out.println("Executor stopped");
            executor.shutdown();
        }
    }
}
