package com.learningpath.snippets.concurrency.producer;

import static java.lang.String.format;

import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * The producer.
 *
 * @author George
 */
public class Producer implements Callable<String> {

    private String id;
    private String[] bucket;
    private Lock lock;
    private Condition consumeNewMessage;
    private Condition produceNewMassage;

    public Producer(final String[] b, final Lock l, final Condition consumeNewMessage, final Condition produceNewMassage) {
        this.bucket = b;
        this.lock = l;
        this.consumeNewMessage = consumeNewMessage;
        this.produceNewMassage = produceNewMassage;
        this.id = UUID.randomUUID().toString();
    }

    @Override
    public String call() throws InterruptedException {
        int count = 10;

        while (count > 0) {
            try {
                lock.lock();
                while (isFull(bucket)) {
                    produceNewMassage.await();
                }
                bucket[0] = "produced-" + count;
                System.out.println(
                        format("{Thread: %s} | {id: %s} Produced message={%s}", Thread.currentThread().getName(), id,
                                bucket[0]));
                count--;
                consumeNewMessage.signalAll();
            } finally {
                lock.unlock();
            }
        }

        return format("Producer %s finished", id);
    }

    private boolean isFull(final String[] bucket) {
        return bucket[0] != null;
    }
}
